<?php

use Illuminate\Support\Facades\Route;

Route::domain('promo.devrules.ru')->group(function () {
    Route::get('/', function (){
        return 'bla bla bla';
    });
});
Route::domain('devrules.ru')->group(function () {
    Route::view('/', 'frontend.index')->name('home');

    Route::post('sendMessageToTelegram', 'SendToTelegramController');

    Route::get('notes', 'NotesController@indexFront')->name('notes');
    Route::get('notes/{note}', 'NotesController@showFront')->name('note.page');

    Route::get('myportfolio', 'PortfolioController@indexFront')->name('portfolio.front');
    Route::get('myportfolio/{item}', 'PortfolioController@showFront')->name('portfolio.page');

    Route::get('projects', 'ProjectsController@indexFront')->name('projects');
    Route::get('projects/{project}', 'ProjectsController@showFront')->name('project.page');

    Route::resource('notez', 'NotesController')->middleware('role:admin');
    Route::post('notez/image', 'NotesController@imageTempStore')->name('notez.imageTempStore');
    Route::post('notez/imageSave', 'NotesController@imageStore')->name('notez.imageSave');

    Route::resource('portfolioz', 'PortfolioController')->middleware('role:admin');

    Route::resource('projectes', 'ProjectsController')->middleware('role:admin');

    Auth::routes();
});