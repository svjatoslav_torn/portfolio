@extends('frontend/layouts/auth')
{{-- @section('css')
    <link rel="stylesheet" href="/css/admin_custom.css">
@stop--}}
@section('content')
 
<div class="container mx-auto pt-4 px-4 py-16 text-black">
	<div class="container p-24">
	
	<div id="app" style="overflow-x:auto;">
		<table class="w-full shadow-lg rounded">
			<thead>
				<tr class="text-left bg-gray-300 border-b border-grey uppercase ">
          <th class="text-sm text-gray-700 p-3">Name</th>
          {{-- <th class="hidden md:table-cell  text-sm text-gray-700">Mobile</th> --}}
          <th class="hidden md:table-cell text-sm text-gray-700">Email</th>
          <th></th>
		    </tr>
			</thead>
	        <tbody class="bg-white">
                @foreach ($users as $user)
                    <tr class="border-b border-grey-light hover:bg-gray-100">
                    
                    <td class="px-2 flex inline-flex items-center">
                        <span>
                            <img
                            class="hidden mr-1 md:mr-2 md:inline-block h-8 w-8 rounded-full object-cover"
                            src="https://images.unsplash.com/photo-1438761681033-6461ffad8d80?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=facearea&facepad=4&w=144&h=144&q=80"
                            alt=""
                            />
                        </span>
                        <span class="py-3 w-40">
                            <p class="text-gray-800 text-sm">{{$user->name}}</p>
                            {{-- <p class="md:hidden text-xs text-gray-600 font-medium">244224317</p> --}}
                                <p class="hidden md:table-cell text-xs text-gray-500 font-medium">
                                    @foreach($user->roles as $r)
                                        {{ $r->display_name }}
                                    @endforeach
                                </p>
                        </span>
                    </td>
                    {{-- <td class="px-2 hidden md:table-cell">
                        <p class="text-sm text-gray-800 font-medium">244224317</p>
                        <p class="text-xs text-gray-500 font-medium"></p>
                    </td> --}}
                    <td class="px-2 hidden md:table-cell">
                        <p class="text-sm text-gray-700 font-medium">{{$user->email}}</p>
                    </td>
                    <td>
                        
                    </td>
                    </tr>
                    
                @endforeach
		      	
	     </tbody>
	    </table>
	</div>
</div>
</div>


    <main role="main" class="col-md-12 ml-sm-auto pt-3 px-4">
      
      
      {{-- <a class="btn btn-sm btn-primary" href="{{route('users.create') }}">@lang('rbac.b_add_new_user')</a>
--}}
<a class="btn btn-sm btn-primary" href="{{ route('users.create') }}">Create</a>
<h2>{{ $title }}</h2>
<div class="table-responsive">
    <table class="table table-striped table-sm">
        <thead>
            <tr>
                <th>@lang('rbac.user_table_username')</th>
                <th>@lang('rbac.user_table_email')</th>
                <th>@lang('rbac.user_table_role')</th>
                <th>@lang('action')</th>
            </tr>
        </thead>
        <tbody>
            @foreach($users as $user)
                <tr>
                    <td>{{ $user->name }}</td>
                    <td>{{ $user->email }}</td>
                    <td>
                        @foreach($user->roles as $r)
                            {{ $r->display_name }}
                        @endforeach
                    </td>
                    @php
                        //var_dump($user->id);die;
                    @endphp
                    <td>
                        <div class="btn-group">
                            <a class="btn btn-primary"
                                href="{{ route('users.edit', ['user' => $user->id]) }}"
                                class="btn btn-info btn-xs"><i class="fas fa-pencil-alt" title="@lang('rbac.role')"></i>
                            </a>
                            <a class="btn btn-danger"
                                href="{{ route('users.show', ['user' => $user->id]) }}"
                                class="btn btn-danger btn-xs"><i class="fas fa-trash" title="@lang('rbac.delete')"></i>
                            </a>
                        </div>
                    </td>
                </tr>
            @endforeach
        </tbody>
    </table>
    {{ $users->links() }}
</div>
</main>
</div>
</div>
@endsection
{{-- @section('js')
    {!! Toastr::message() !!} 
@stop--}}
