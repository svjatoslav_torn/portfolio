@extends('frontend.layouts.main', [
    'title' => 'Заметки о программировании, примеры реализаций, обзоры технологий',
    'description' => 'Я Святослав Торн, и это мой блог. Здесь можно найти список моих заметок о веб-разработке, интересных решениях. Выбирите заметку для продолжения',
    'keywords' => 'Программирование, рецепты php, laravel, yii2, блог разработчика, Святослав Торн',
])

@section('content')
    <style>
        .gradient{
            background: rgb(131,131,131);
            background: linear-gradient(90deg, rgba(131,131,131,1) 0%, rgba(209,209,209,0.7010154403558299) 36%, rgba(142,142,142,1) 100%);
        }
    </style>
    <span id="navFix" class="hidden"></span>
    <!--Hero-->
    <div class="pt-6 md:pt-24">       

        <div class="container px-1 md:px-3 mx-auto flex flex-wrap flex-col md:flex-row items-center">             
            <div class="flex flex-wrap justify-between pt-6">
                
                @foreach ($notes as $note)
                    <div class="w-full {{ $loop->index !== 0 ? $loop->index !== 2 ? $loop->index !== 3 ? 'md:w-1/2 lg:w-1/3' : 'md:w-1/2 lg:w-1/2' : 'md:w-1/2 lg:w-1/2' : 'md:w-1/2 lg:w-2/3' }} py-4 px-0 md:p-6 sm:pb-1 flex flex-col flex-grow flex-shrink">
                        <div class="flex-1 bg-white rounded-t rounded-b-none overflow-hidden shadow-lg">
                            <a href="{{route('note.page', $note->slug)}}" class="{{$note->img_src === null ? 'pt-6' : ''}} flex flex-wrap no-underline hover:no-underline">
                                @if($note->img_src !== null)
                                    <img src="{{$note->img_src}}" class="h-full w-full rounded-t pb-6">
                                @endif
                                <p class="w-full text-gray-600 text-xs md:text-sm px-6">{{$note->main_tag}}</p>
                                <div class="w-full font-bold text-xl text-gray-900 px-6">{{$note->title}}</div>
                                <p class="text-gray-800 font-serif text-lg px-6 mb-5 pt-2">
                                    {{$note->preview}}
                                </p>
                            </a>
                        </div>
                        <div class="flex-none mt-auto bg-white rounded-b rounded-t-none overflow-hidden shadow-lg p-6">
                            <div class="flex items-center justify-between">
                                <div class="flex flex-row flex-no-wrap items-center">
                                    <img class="w-8 h-8 rounded-full mr-2 avatar" data-tippy-content="Author Name" src="/img/profile-img.jpeg" alt="Avatar of Author" tabindex="0">
                                    <p class="text-gray-600 text-lg mr-4">Святослав Торн</p>
                                </div>
                                <p class="text-gray-600 text-lg">{{$note->read_time}} min read</p>
                            </div>
                        </div>
                    </div>
                @endforeach

               
                {{-- <div class="w-full p-2 flex flex-col flex-grow flex-shrink px-64 py-5"> --}}
                    {{-- <a href="#" class="bg-white rounded shadow-lg text-center w-full text-black p-5 hover:bg-gray-900 hover:text-gray-100">Next</a> --}}
                {{-- </div> --}}

            </div>
        </div>
    </div>
    

   
@endsection
