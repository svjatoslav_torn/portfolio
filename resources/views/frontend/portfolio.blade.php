@extends('frontend.layouts.main', [
    'title' => 'Портфолио | Святослав Торн | Список выполненных заказов',
    'description' => 'Я Святослав Торн, и это мое портфолио. Тут можно найти выполненные мною заказы, а также заказать похожие реализации. ',
    'keywords' => 'Блог, веб-разработчик, портфолио, фриланс, выполню заказ, Святослав Торн',
])

@section('content')
    <style>
        .gradient{
            background: rgb(131,131,131);
            background: linear-gradient(90deg, rgba(131,131,131,1) 0%, rgba(209,209,209,0.7010154403558299) 36%, rgba(142,142,142,1) 100%);
        }
    </style>
    <span id="navFix" class="hidden"></span>

    <section class="gradient pt-24">

        <div class="container mx-auto flex items-center flex-wrap pt-4 pb-12">

            @foreach ($portfolio as $work)
                
            <div class="w-full md:w-1/3 xl:w-1/4 p-6 flex flex-col">
                <a href="{{route('portfolio.page', $work->id)}}">
                    <img class="hover:grow hover:shadow-lg" src="/{{$work->main_img}}">
                    <div class="pt-3 flex items-center justify-between">
                        <p class="">{{$work->name}}</p>
                    </div>
                </a>
            </div>
            
            @endforeach
        </div>

    </section>


@endsection