@extends('frontend.layouts.main', [
    'title' => 'Хобби проекты | Список реализованных проектов',
    'description' => 'Я Святослав Торн, и это мой блог. Список реализованных проектов, от идеи до продакшен-релиза. С каждым проектом можно ознакомиться отдельно. Разработаю Landing page',
    'keywords' => 'Блог, веб-разработка, список проектов, мои работы, Святослав Торн',
])

@section('content')
    <style>
        .gradient{
            background: rgb(131,131,131);
            background: linear-gradient(90deg, rgba(131,131,131,1) 0%, rgba(209,209,209,0.7010154403558299) 36%, rgba(142,142,142,1) 100%);
        }
    </style>
    <span id="navFix" class="hidden"></span>

    <section class="gradient pt-12 md:pt-24 mt-2">

        <div class="container mx-auto flex items-center flex-wrap pt-4 pb-12">

            @foreach ($projects as $project)
                
            <div class="w-full md:w-1/2 xl:w-1/3 p-3 md:p-6 flex flex-col">
                <a href="{{route('project.page', $project->slug)}}" class="bg-white  hover:grow hover:shadow-lg">
                    <img class="w-full h-auto" src="/{{$project->main_img}}">
                    <div class="p-3 flex items-center justify-between">
                        <p class="text-black font-bold">{{$project->name}}</p>
                        <p class="text-gray-900 opacity-50">{{ \Str::words($project->tech, 3, '')}}</p>
                    </div>
                </a>
            </div>
            
            @endforeach
        </div>

    </section>


@endsection