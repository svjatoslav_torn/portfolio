@extends('frontend.layouts.main', [
    'title' => 'Святослав Торн | Разработка landing page и web-приложений в России',
    'description' => 'Я Святослав Торн, автор блога-визитки dev.rules. На ресурсе вы найдете, информацию о знаниях и скилах, сможете ознакомиться с выполнеными работами, а также найти интересные заметки',
    'keywords' => 'Программирование, рецепты php, laravel, yii2, блог разработчика',
])

@section('content')
<style>
    .gradient {
        background: linear-gradient(90deg, #d53369 0%, #daae51 100%);
    }
    .open {
		visibility: visible;
		opacity: 1;
	}
</style>

    <!--Hero-->
    <div class="pt-24">

        <div class="container px-3 mx-auto flex flex-wrap flex-col md:flex-row items-center">
            <!--Left Col-->
            <div class="flex flex-col w-full md:w-2/5 justify-center items-start text-center md:text-left">
                <p class="uppercase tracking-loose w-full">У вас есть идея?</p>
                <h1 class="my-4 text-5xl font-bold leading-tight"><strong>Разработка</strong> сайтов</h1>
                <p class="leading-normal text-2xl mb-8">Разработка настоящих веб-приложений и Landing Page!</p>
                 <button class="mx-auto lg:mx-0 hover:underline bg-white text-gray-800 font-bold rounded-full my-6 py-4 px-8 shadow-lg">Оставить заявку</button>
            </div>
            <!--Right Col-->
            <div class="w-full md:w-3/5 py-6 text-center">
                <img class="w-full md:w-4/5 z-50" src="/img/hero.png">
            </div>
            
        </div>
    </div>

    <div class="relative -mt-12 lg:-mt-24">
        <svg viewBox="0 0 1428 174" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
            <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                <g transform="translate(-2.000000, 44.000000)" fill="#FFFFFF" fill-rule="nonzero">
                    <path d="M0,0 C90.7283404,0.927527913 147.912752,27.187927 291.910178,59.9119003 C387.908462,81.7278826 543.605069,89.334785 759,82.7326078 C469.336065,156.254352 216.336065,153.6679 0,74.9732496" opacity="0.100000001"></path>
                    <path d="M100,104.708498 C277.413333,72.2345949 426.147877,52.5246657 546.203633,45.5787101 C666.259389,38.6327546 810.524845,41.7979068 979,55.0741668 C931.069965,56.122511 810.303266,74.8455141 616.699903,111.243176 C423.096539,147.640838 250.863238,145.462612 100,104.708498 Z" opacity="0.100000001"></path>
                    <path d="M1046,51.6521276 C1130.83045,29.328812 1279.08318,17.607883 1439,40.1656806 L1439,120 C1271.17211,77.9435312 1140.17211,55.1609071 1046,51.6521276 Z" id="Path-4" opacity="0.200000003"></path>
                </g>
                <g transform="translate(-4.000000, 76.000000)" fill="#FFFFFF" fill-rule="nonzero">
                    <path d="M0.457,34.035 C57.086,53.198 98.208,65.809 123.822,71.865 C181.454,85.495 234.295,90.29 272.033,93.459 C311.355,96.759 396.635,95.801 461.025,91.663 C486.76,90.01 518.727,86.372 556.926,80.752 C595.747,74.596 622.372,70.008 636.799,66.991 C663.913,61.324 712.501,49.503 727.605,46.128 C780.47,34.317 818.839,22.532 856.324,15.904 C922.689,4.169 955.676,2.522 1011.185,0.432 C1060.705,1.477 1097.39,3.129 1121.236,5.387 C1161.703,9.219 1208.621,17.821 1235.4,22.304 C1285.855,30.748 1354.351,47.432 1440.886,72.354 L1441.191,104.352 L1.121,104.031 L0.457,34.035 Z"></path>
                </g>
            </g>
        </svg>
    </div>

    {{-- Timeline section --}}
    <section class="bg-white border-b py-8">
        <div class="container max-w-5xl mx-auto m-8">
            <h1 class="w-full my-2 text-5xl font-bold leading-tight text-center text-gray-800">Слушаю, Кэп?</h1>
            
            <div class="w-full mb-4">	
                <div class="h-1 mx-auto gradient w-64 opacity-25 my-0 py-0 rounded-t"></div>
            </div>
        {{-- First --}}
            <div class="flex flex-wrap items-center ">
                <div class="w-5/6 sm:w-1/2 p-6">
                    <h3 class="text-3xl text-gray-800 font-bold leading-none mb-3">Адаптивный дизайн</h3>
                    <p class="text-gray-600 mb-8">
                        Ваш проект будет адаптирован под все устройства. Использую современные решения для создания дизайна. TailWind CSS - последнее слово в мире веб-дизайна, уникальные, адаптивные шаблоны. А также Bootstrap 4, MaterializeCSS.
                    </p>                    
                </div>
                <div class="w-full sm:w-1/2 p-6">
                    <img class="w-full sm:h-64 mx-auto" src="/img/responsive_disign.svg" alt="Responsive image">      
                </div>
            </div>
            
            {{-- Second --}}
            <div class="flex flex-wrap items-center flex-col-reverse sm:flex-row">	
                <div class="w-full sm:w-1/2 p-6 mt-6">
                    <img class="w-full sm:h-64 mx-auto" src="/img/laravel.svg" alt="Laravel | Yii2 image">
                </div>
                <div class="w-full sm:w-1/2 p-6 mt-6">
                    <div class="align-middle">
                        <h3 class="text-3xl text-gray-800 font-bold leading-none mb-3">Современные решения</h3>
                        <p class="text-gray-600 mb-8">Выбирая меня, вы получаете продукт написанный на таких PHP Framework'ах как Laravel, Yii2 (Yii3). Такие проекты легко расширять и модернизировать. Веб-приложение будет быстро и надежно работать.</p>
                    </div>
                </div>    
            </div>

            {{-- Three --}}
            <div class="flex flex-wrap items-center">
                <div class="w-5/6 sm:w-1/2 p-6">
                    <h3 class="text-3xl text-gray-800 font-bold leading-none mb-3">Поддержка</h3>
                    <p class="text-gray-600 mb-8">
                        Буду поддерживать ваше веб-приложение. Мне не стыдно за код который пишу. Я готов его поддерживать и модернизировать согласно вашим бизнес-задачам.
                    </p>                    
                </div>
                <div class="w-full sm:w-1/2 p-6">
                    <img class="w-full sm:h-64 mx-auto" src="/img/support.svg" alt="Responsive image">      
                </div>
            </div>

            {{-- For --}}
            <div class="flex flex-wrap items-center  sm:flex-row">	
                <div class="w-full sm:w-1/2 p-6 mt-6">
                    <img class="w-full sm:h-64 mx-auto" src="/img/earth.svg" alt="About image">
                </div>
                <div class="w-full sm:w-1/2 p-6 mt-6">
                    <div class="align-middle">
                        <h3 class="text-3xl text-gray-800 font-bold leading-none mb-3">Из любой точки мира</h3>
                        <p class="text-gray-600 mb-8">Вы можете находится где угодно, и говорить на любом языке. Это не помешает нашему сотрудничеству.</p>
                    </div>
                </div>    
            </div>

        </div>
    </section>

    <section class="bg-white border-b py-8">
	
        <div class="container mx-auto flex flex-wrap pt-4 pb-12">
        
            <h1 class="w-full my-2 text-5xl font-bold leading-tight text-center text-gray-800">Решения</h1>
            <div class="w-full mb-4">	
                <div class="h-1 mx-auto gradient w-64 opacity-25 my-0 py-0 rounded-t"></div>
            </div>
        
            <div class="w-full md:w-1/3 p-6 flex flex-col flex-grow flex-shrink">
                <div class="flex-1 bg-white rounded-t rounded-b-none overflow-hidden shadow">
                    <a href="#" class="flex flex-wrap no-underline hover:no-underline">
                        <p class="w-full text-gray-600 text-xs md:text-sm px-6 text-right">от 1 дня</p>
                        <div class="w-full font-bold text-2xl text-gray-800 px-6 text-center">Landing page</div>
                        <p class="text-gray-800 text-base px-6 mb-5 mt-4">
                            Одностраничный сайт или сайт визитка для вашего бизнеса. SEO оптимизация. Установка плагинов для повышения конверсий.
                        </p>
                    </a>
                </div>
                <div class="flex-none mt-auto bg-white rounded-b rounded-t-none overflow-hidden shadow p-6">
                    <div class="flex items-center justify-center">
                        <button class="mx-auto lg:mx-0 hover:underline gradient text-white font-bold rounded-full my-6 py-4 px-8 shadow-lg">Заказать</button>
                    </div>
                </div>
            </div>
            
            
            
            <div class="w-full md:w-1/3 p-6 flex flex-col flex-grow flex-shrink">
                <div class="flex-1 bg-white rounded-t rounded-b-none overflow-hidden shadow">
                    <a href="#" class="flex flex-wrap no-underline hover:no-underline">
                        <p class="w-full text-gray-600 text-xs md:text-sm px-6 text-right">от 7 дней</p>
                        <div class="w-full font-bold text-2xl text-gray-800 px-6 text-center">Интернет магазин</div>
                        <p class="text-gray-800 text-base px-6 mb-5 mt-4">
                            Полнофункциональный интернет-магазин, адаптивный дизайн, функционал разрабатывается под ваши нужды. Никаких типовых проектов.
                        </p>
                    </a>
                </div>
                <div class="flex-none mt-auto bg-white rounded-b rounded-t-none overflow-hidden shadow p-6">
                    <div class="flex items-center justify-center">
                        <button class="mx-auto lg:mx-0 hover:underline gradient text-white font-bold rounded-full my-6 py-4 px-8 shadow-lg">Заказать</button>
                    </div>
                </div>
            </div>
            
            
            
            <div class="w-full md:w-1/3 p-6 flex flex-col flex-grow flex-shrink">
                <div class="flex-1 bg-white rounded-t rounded-b-none overflow-hidden shadow">
                    <a href="#" class="flex flex-wrap no-underline hover:no-underline">
                        <p class="w-full text-gray-600 text-xs md:text-sm px-6 text-right">от 7 дней</p>
                        <div class="w-full font-bold text-2xl text-gray-800 px-6 text-center">CRM | Office</div>
                        <p class="text-gray-800 text-base px-6 mb-5 mt-4">
                           Разработка CRM системы для вашего бизнеса. Самый сложный вид разработки. Полное соответствие вашему корпоративному стилю, и вашим задачам. 
                        </p>
                    </a>
                </div>
                <div class="flex-none mt-auto bg-white rounded-b rounded-t-none overflow-hidden shadow p-6">
                    <div class="flex items-center justify-center">
                        <button class="mx-auto lg:mx-0 hover:underline gradient text-white font-bold rounded-full my-6 py-4 px-8 shadow-lg">Заказать</button>
                    </div>
                </div>
            </div>
            
            
        </div>
    
    </section>

    <section class="bg-gray-100 py-8">	
        <div class="container mx-auto px-2 pt-4 pb-12 text-gray-800">
    
            <h1 class="w-full my-2 text-5xl font-bold leading-tight text-center text-gray-800">Навыки</h1>
            <div class="w-full mb-4">	
                <div class="h-1 mx-auto gradient w-64 opacity-25 my-0 py-0 rounded-t"></div>
            </div>
        
        
            
            <div class="flex flex-col sm:flex-row justify-center pt-12 my-12 sm:my-4">
                
                <div class="flex flex-col w-5/6 lg:w-1/4 mx-auto lg:mx-0 rounded-none lg:rounded-l-lg bg-white mt-4">
                    <div class="flex-1 bg-white text-gray-600 rounded-t rounded-b-none overflow-hidden shadow">
                        <div class="p-8 text-3xl font-bold text-center border-b-4">Веб-дизайн</div>
                        <ul class="w-full text-center text-sm">
                            <li class="border-b py-4">Разработка прототипа</li>
                            <li class="border-b py-4">Создание прототипа в Figma</li>
                            <li class="border-b py-4">Верстка макета с прототипа</li>
                            <li class="border-b py-4">Верстка с Landing page на Tilda</li>
                            <li class="border-b py-4">Копирование Landing page</li>
                        </ul>
                    </div>
                    <div class="flex-none mt-auto bg-white rounded-b rounded-t-none overflow-hidden shadow p-6">
{{--                        <div class="w-full pt-6 text-3xl text-gray-600 font-bold text-center">от 500 <span class="text-2xl">₽/час</span></div>--}}
                        <div class="flex items-center justify-center">
                            <button class="mx-auto lg:mx-0 hover:underline gradient text-white font-bold rounded-full my-6 py-4 px-8 shadow-lg">Подробнее</button>
                        </div>
                    </div>
                </div>
                
                
                
                <div class="flex flex-col w-5/6 lg:w-1/3 mx-auto lg:mx-0 rounded-lg bg-white mt-4 sm:-mt-6 shadow-lg z-10">
                    <div class="flex-1 bg-white rounded-t rounded-b-none overflow-hidden shadow">
                        <div class="w-full p-8 text-3xl font-bold text-center">Разработка под ключ</div>
                        <div class="h-1 w-full gradient my-0 py-0 rounded-t"></div>
                        <ul class="w-full text-center text-base font-bold">
                            <li class="border-b py-4">Landing page</li>
                            <li class="border-b py-4">Сайт визитка для компании</li>
                            <li class="border-b py-4">Блог</li>
                            <li class="border-b py-4">Информационный портал</li>
                            <li class="border-b py-4">CRM система</li>
                            <li class="border-b py-4">Office application</li>
                        </ul>					
                    </div>
                    <div class="flex-none mt-auto bg-white rounded-b rounded-t-none overflow-hidden shadow p-6">
{{--                        <div class="w-full pt-6 text-4xl font-bold text-center">от 3000 <span class="text-2xl">₽</span></div>--}}
                        <div class="flex items-center justify-center">
                            <button class="mx-auto lg:mx-0 hover:underline gradient text-white font-bold rounded-full my-6 py-4 px-8 shadow-lg">Подробнее</button>
                        </div>
                    </div>
                </div>
                
                
                
                <div class="flex flex-col w-5/6 lg:w-1/4 mx-auto lg:mx-0 rounded-none lg:rounded-l-lg bg-white mt-4">
                    <div class="flex-1 bg-white text-gray-600 rounded-t rounded-b-none overflow-hidden shadow">
                        <div class="p-8 text-3xl font-bold text-center border-b-4">Веб-разработка</div>
                        <ul class="w-full text-center text-sm">
                            <li class="border-b py-4">Чистый PHP</li>
                            <li class="border-b py-4">Yii 2 framework</li>
                            <li class="border-b py-4">Laravel framework</li>
                            <li class="border-b py-4">Знание библиотек</li>
                        </ul>
                    </div>
                    <div class="flex-none mt-auto bg-white rounded-b rounded-t-none overflow-hidden shadow p-6">
{{--                        <div class="w-full pt-6 text-3xl text-gray-600 font-bold text-center">от 600 <span class="text-2xl">₽/час</span></div>--}}
                        <div class="flex items-center justify-center">
                            <button class="mx-auto lg:mx-0 hover:underline gradient text-white font-bold rounded-full my-6 py-4 px-8 shadow-lg">Подробнее</button>
                        </div>
                    </div>
                </div>
    
            </div>
            
        </div>
            
            
    </section>

    
<!-- Change the colour #f8fafc to match the previous section colour -->
<svg class="wave-top" viewBox="0 0 1439 147" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
	<g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
		<g transform="translate(-1.000000, -14.000000)" fill-rule="nonzero">
			<g class="wave" fill="#f8fafc">
				<path d="M1440,84 C1383.555,64.3 1342.555,51.3 1317,45 C1259.5,30.824 1206.707,25.526 1169,22 C1129.711,18.326 1044.426,18.475 980,22 C954.25,23.409 922.25,26.742 884,32 C845.122,37.787 818.455,42.121 804,45 C776.833,50.41 728.136,61.77 713,65 C660.023,76.309 621.544,87.729 584,94 C517.525,105.104 484.525,106.438 429,108 C379.49,106.484 342.823,104.484 319,102 C278.571,97.783 231.737,88.736 205,84 C154.629,75.076 86.296,57.743 0,32 L0,0 L1440,0 L1440,84 Z"></path>
			</g>
			<g transform="translate(1.000000, 15.000000)" fill="#FFFFFF">
				<g transform="translate(719.500000, 68.500000) rotate(-180.000000) translate(-719.500000, -68.500000) ">
					<path d="M0,0 C90.7283404,0.927527913 147.912752,27.187927 291.910178,59.9119003 C387.908462,81.7278826 543.605069,89.334785 759,82.7326078 C469.336065,156.254352 216.336065,153.6679 0,74.9732496" opacity="0.100000001"></path>
					<path d="M100,104.708498 C277.413333,72.2345949 426.147877,52.5246657 546.203633,45.5787101 C666.259389,38.6327546 810.524845,41.7979068 979,55.0741668 C931.069965,56.122511 810.303266,74.8455141 616.699903,111.243176 C423.096539,147.640838 250.863238,145.462612 100,104.708498 Z" opacity="0.100000001"></path>
					<path d="M1046,51.6521276 C1130.83045,29.328812 1279.08318,17.607883 1439,40.1656806 L1439,120 C1271.17211,77.9435312 1140.17211,55.1609071 1046,51.6521276 Z" opacity="0.200000003"></path>
				</g>
			</g>
		</g>
	</g>
</svg>

<section class="container mx-auto text-center py-6 mb-12">

	<h1 class="w-full my-2 text-5xl font-bold leading-tight text-center text-white popup-open">Написать мне</h1>
	<div class="w-full mb-4">	
		<div class="h-1 mx-auto bg-white w-1/6 opacity-25 my-0 py-0 rounded-t"></div>
	</div>

	<h3 class="my-4 text-3xl leading-tight">You idea - my work!</h3>	

	<button id="contactForm" class="mx-auto lg:mx-0 hover:underline bg-white text-gray-800 font-bold rounded-full my-6 py-4 px-8 shadow-lg">Написать</button>
		
</section>

<div id="modalContactForm"
    style="background-color: rgba(0,0,0,.3); transition: all 0.5s ease;" 
    class="invisible opacity-0  fixed top-0 left-0 w-full h-full flex items-center shadow-lg overflow-y-auto z-50">
    <div class="w-full sm:w-2/3 md:w-2/3 xl:w-2/4 mx-auto lg:px-32 rounded-lg overflow-y-auto">
        <div class="">
            <div class="flex justify-end pr-4 pt-2">
                <button id="closeContactForm" class="text-4xl leading-none hover:text-gray-300">&times;</button>
            </div>
            <div class="responsive-container overflow-hidden relative" style="">
                <form id="contact-me" onsubmit="sendMessage()" action="javascript:void(null);" method="POST" class="w-full mx-auto max-w-3xl bg-white shadow p-8 text-gray-700 ">


                    <h2 class="w-full my-2 text-3xl font-bold leading-tight my-5">Напишите мне</h2>
                    <!-- name field -->
                    <div class="flex flex-wrap mb-3">
                        <div class="relative w-full appearance-none label-floating">
                            <input class="tracking-wide py-2 px-4 mb-3 leading-relaxed appearance-none block w-full bg-gray-200 border border-gray-200 rounded focus:outline-none focus:bg-white focus:border-gray-500"
                            name="name" type="text" placeholder="Ваше Имя" required>
                        </div>
                    </div>
                    <!-- email field -->
                    <div class="flex flex-wrap mb-3">
                        <div class="relative w-full appearance-none label-floating">
                            <input class="tracking-wide py-2 px-4 mb-3 leading-relaxed appearance-none block w-full bg-gray-200 border border-gray-200 rounded focus:outline-none focus:bg-white focus:border-gray-500"
                            name="contact" type="text" placeholder="E-mail или @telegram" required>
                        </div>
                    </div>
                    <!-- Message field -->
                    <div class="flex flex-wrap mb-3">
                        <div class="relative w-full appearance-none label-floating">
                            <textarea class="autoexpand tracking-wide py-2 px-4 mb-3 leading-relaxed appearance-none block w-full bg-gray-200 border border-gray-200 rounded focus:outline-none focus:bg-white focus:border-gray-500"
                                name="message" type="text" placeholder="Все что хотите..."></textarea>
                            </label>
                        </div>
                    </div>
                
                    <div class="">
                        <button class="w-full shadow bg-teal-400 hover:bg-teal-400 focus:shadow-outline focus:outline-none text-white font-bold py-2 px-4 rounded"
                            type="submit">
                            Отправить
                        </button>
                
                    </div>
                </form>
            </div>
            
        </div>
    </div>
</div>


@endsection
@section('js')
    
<script>
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': '{{ csrf_token() }}'
        }
    });
    function sendMessage(){
          $.ajax({
            type: "POST",
            url: "{{ url('sendMessageToTelegram') }}",
            data: {
              data: $('form#contact-me').serializeArray(),
            },
            success: function (data) {
                $('#modalContactForm').toggleClass('open');
                $('#modalContactForm').find("input[type=text], textarea").val("");
                if(data){
                    swal("Спасибо! Я получил Ваше сообщение", {
                        buttons: false,
                        timer: 1500,
                    });
                }
                console.log(data)
            },
            // error: function (e) {
            //     console.log(e)
            // }
          })
        // console.log('Hi')
    }
        $(document).ready(function($) {
            $('#contactForm').click(function() {
                $('#modalContactForm').toggleClass('open');
                return false;
            });	
        
            $('#closeContactForm').click(function() {
                $(this).parents('#modalContactForm').toggleClass('open');
                return false;
            });		
     
            $(document).keydown(function(e) {
                if (e.keyCode === 27) {
                    e.stopPropagation();
                    $('#modalContactForm').toggleClass('open');
                }
            });
        });
    </script>
    @endsection