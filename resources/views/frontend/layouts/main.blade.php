<!DOCTYPE html>
<html lang="ru">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>@isset($title){{$title}} | @endisset DEV.RULES</title>

    <meta name="author" content="Svjatoslav Torn">
    <meta name="description" content="@isset($description){{$description}}@endisset">
    <meta name="keywords" content="@isset($keywords){{$keywords}}@endisset">

    @if(env('APP_ENV') == 'production')
        <meta name="yandex-verification" content="d11c326f6e78d0e3" />
        <!-- Yandex.Metrika counter --> <script type="text/javascript" > (function(m,e,t,r,i,k,a){m[i]=m[i]||function(){(m[i].a=m[i].a||[]).push(arguments)}; m[i].l=1*new Date();k=e.createElement(t),a=e.getElementsByTagName(t)[0],k.async=1,k.src=r,a.parentNode.insertBefore(k,a)}) (window, document, "script", "https://mc.yandex.ru/metrika/tag.js", "ym"); ym(62517976, "init", { clickmap:true, trackLinks:true, accurateTrackBounce:true, webvisor:true }); </script> <noscript><div><img src="https://mc.yandex.ru/watch/62517976" style="position:absolute; left:-9999px;" alt="" /></div></noscript> <!-- /Yandex.Metrika counter -->
    @endif
    <link rel="stylesheet" href="/css/main.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.13.0/css/all.min.css">
    <link href="/hljs/styles/monokai-sublime.css" rel="stylesheet" />
    {{-- <link href="https://cdnjs.cloudflare.com/ajax/libs/prism/1.20.0/themes/prism-okaidia.min.css" rel="stylesheet" /> --}}
    <link href="https://fonts.googleapis.com/css2?family=Nunito:wght@200;300;400;600;700;800;900&display=swap" rel="stylesheet">
</head>
<body  class="leading-normal tracking-normal text-white gradient" style="font-family: 'Nutino', sans-serif;">
    
    @include('frontend.layouts.navigation')
    @include('frontend.layouts.admin_nav')
    @yield('content')
    @include('frontend.layouts.footer')
    {{-- <span class="hidden sm:bg-white" id="navMobFix"></span> --}}
    <script src="/js/jquery.js"></script>
    <script src="/js/app.js"></script>
    <script src="/js/main.js"></script>
    {{-- <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.5.0/jquery.min.js"></script> --}}
    <script src="/hljs/highlight.pack.js"></script>
    <script>hljs.initHighlightingOnLoad();</script>
    @yield('js')
</body>
</html>