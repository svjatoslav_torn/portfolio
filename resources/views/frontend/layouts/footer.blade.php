<!--Footer-->
<footer class="bg-white">
	<div class="container mx-auto  px-8">

        <div class="w-full flex flex-wrap pt-4 items-center justify-center">
			<a class="w-full text-gray-500 no-underline hover:no-underline font-bold text-2xl lg:text-2xl flex items-center justify-center"  href="{{ route('home') }}"> 
                <i class="text-xl md:text-3xl mr-2 fas fa-laptop-code"></i> DEV.RULES
            </a>
            <p class="w-full text-gray-500 flex text-xl justify-center">sefkiss.torn@yandex.ru</p>
            <p class="w-full text-gray-500 flex text-sm justify-center">&copy Все права защищены {{date('Y')}}</p>		         
            
        </div>
    </div>
	
	<a href="https://www.freepik.com/free-photos-vectors/background" class="bg-transparent text-gray-300 font-light">Background vector created by freepik - www.freepik.com</a>

</footer>