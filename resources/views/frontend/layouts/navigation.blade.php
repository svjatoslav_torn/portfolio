<!--Nav-->
<nav id="header" class="fixed w-full z-30 top-0 text-white">

	<div class="w-full container mx-auto flex flex-wrap items-center justify-between mt-0 py-2">
			
		<div class="pl-4 flex items-center">
			<a class="toggleColour text-white no-underline hover:no-underline font-bold text-2xl lg:text-4xl flex items-center"  href="{{ route('home') }}">
				<i class="text-xl md:text-3xl mr-2 fas fa-laptop-code"></i> DEV.RULES
			</a>
		</div>

		<div class="block lg:hidden pr-4">
			<button id="nav-toggle" class="flex items-center p-1 text-orange-800 hover:text-gray-900">
				<svg class="fill-current h-6 w-6" viewBox="0 0 20 20" xmlns="http://www.w3.org/2000/svg"><title>Menu</title><path d="M0 3h20v2H0V3zm0 6h20v2H0V9zm0 6h20v2H0v-2z"/></svg>
			</button>
		</div>

		<div class="w-full flex-grow lg:flex lg:items-center lg:w-auto hidden lg:block mt-2 lg:mt-0 bg-white lg:bg-transparent text-black p-4 lg:p-0 z-20" id="nav-content">
			{{-- <div class="flex-1 w-full mx-auto max-w-sm content-center py-4 lg:py-0">
				<div class="relative pull-right pl-4 pr-4 md:pr-0">
				   <input type="search" placeholder="Search" class="w-full bg-gray-100 text-sm text-gray-800 transition border focus:outline-none focus:border-purple-500 rounded py-1 px-2 pl-10 appearance-none leading-normal">
				   <div class="absolute search-icon" style="top: 0.375rem;left: 1.75rem;">
					  <svg class="fill-current pointer-events-none text-gray-800 w-4 h-4" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20">
						 <path d="M12.9 14.32a8 8 0 1 1 1.41-1.41l5.35 5.33-1.42 1.42-5.33-5.34zM8 14A6 6 0 1 0 8 2a6 6 0 0 0 0 12z"></path>
					  </svg>
				   </div>
				</div>
			 </div> --}}
			<ul class="list-reset lg:flex justify-end flex-1 items-center">
				
				<li class="mr-3 text-center">
					<a class="{{ Request::is('home') ? 'bg-gray-200 cursor-default' : 'hover:text-gray-700 hover:text-underline'}} clickk inline-block text-white font-bold  no-underline py-4 md:-my-4 md:py-6 px-6" 
						href="{{ route('home') }}">
						Главная</a>
				</li>
				<li class="mr-3 text-center">
					<a class="{{ Request::is(['projects', 'projects/*']) ? 'bg-gray-200 cursor-default' : 'hover:text-gray-700 hover:text-underline'}} clickk inline-block text-white font-bold  no-underline py-4 md:-my-4 md:py-6 px-6" 
						href="{{ route('projects') }}">
						Проекты</a>
				</li>
				<li class="mr-3 text-center">
					<a class="{{ Request::is(['myportfolio', 'myportfolio/*']) ? 'bg-gray-200 cursor-default' : 'hover:text-gray-700 hover:text-underline'}} clickk inline-block text-white font-bold  no-underline py-4 md:-my-4 md:py-6 px-6" 
						href="{{ route('portfolio.front') }}">
						Портфолио</a>
				</li>
				<li class="mr-3 text-center">
					<a class="{{ Request::is(['notes', 'notes/*']) ? 'bg-gray-200 cursor-default' : 'hover:text-gray-700 hover:text-underline'}} clickk inline-block text-white font-bold  no-underline py-4 md:-my-4 md:py-6 px-6" 
						href="{{ route('notes') }}">
						Заметки</a>
				</li>
				
			</ul>
		</div>
	</div>
	
	<hr class="border-b border-gray-100 opacity-25 my-0 py-0" />
</nav>
