 {{-- Logout form panel --}}
 @if (isset(Auth::user()->name))
 {{-- Left buttons --}}
 @if (Request::is(['notez/*', 'portfolioz/*', 'projectes/*']))
 <div class="absolute  text-gray-700 z-20" style="top: 5rem; left: 10.5rem" >
    <div class="flex flex-row justify-center">
      @if (Request::is(['notez/*']))
        <a href="{{ route('notez.index') }}" class="flex items-center bg-white py-2 px-8 hover:bg-gray-300">
          Назад
        </a>          
      @endif
      @if (Request::is(['portfolioz/*']))
        <a href="{{ route('portfolioz.index') }}" class="flex items-center bg-white py-2 px-8 hover:bg-gray-300">
          Назад
        </a>          
      @endif
      @if (Request::is(['projectes/*']))
        <a href="{{ route('projectes.index') }}" class="flex items-center bg-white py-2 px-8 hover:bg-gray-300">
          Назад
        </a>          
      @endif
    </div>
 </div>
     
 @endif

 {{-- Right buttons --}}
 <div class="absolute  text-gray-700 z-20" style="top: 5rem; right: 8.5rem" >
     <div class="flex flex-row justify-center">
         <div class="flex flex-no-wrap items-center bg-white py-2 px-3 mr-2">
             <img class="flex h-8 w-8 rounded-full mr-2" src="/img/profile-img.jpeg" alt="">
             <p class="flex">{{ Auth::user()->name }}</p>
         </div>

        @if (Request::is(['notes', 'notez']))
          <div class="flex flex-no-wrap items-center bg-white  mr-2 hover:bg-gray-300">
            <a href="{{ route('notez.create') }}" class="flex py-4 px-3"><i class="fas fa-plus"></i></a>
          </div>            
          <div class="flex flex-no-wrap items-center bg-white  mr-2 hover:bg-gray-300">
            <a href="{{ route('notez.index') }}" class="flex py-4 px-3"><i class="fas fa-list"></i></a>
          </div>            
        @endif

        @if (Request::is(['myportfolio', 'portfolioz']))
          <div class="flex flex-no-wrap items-center bg-white mr-2 hover:bg-gray-300">
            <a href="{{ route('portfolioz.create') }}" class="flex py-4 px-3"><i class="fas fa-plus"></i></a>
          </div>            
          <div class="flex flex-no-wrap items-center bg-white mr-2 hover:bg-gray-300">
            <a href="{{ route('portfolioz.index') }}" class="flex py-4 px-3"><i class="fas fa-list"></i></a>
          </div>            
        @endif
        
        @if (Request::is(['projects', 'projectes']))
          <div class="flex flex-no-wrap items-center bg-white mr-2 hover:bg-gray-300">
            <a href="{{ route('projectes.create') }}" class="flex py-4 px-3"><i class="fas fa-plus"></i></a>
          </div>            
          <div class="flex flex-no-wrap items-center bg-white mr-2 hover:bg-gray-300">
            <a href="{{ route('projectes.index') }}" class="flex py-4 px-3"><i class="fas fa-list"></i></a>
          </div>            
        @endif
        {{-- <div class="flex mr-2">
             <div class="group inline-block">
                 <button
                   class="outline-none focus:outline-none px-3 py-3 bg-white flex items-center min-w-32"
                 >
                   <span class="pr-1 font-semibold flex-1">RBAC</span>
                   <span>
                     <svg
                       class="fill-current h-4 w-4 transform group-hover:-rotate-180
                       transition duration-150 ease-in-out"
                       xmlns="http://www.w3.org/2000/svg"
                       viewBox="0 0 20 20"
                     >
                       <path
                         d="M9.293 12.95l.707.707L15.657 8l-1.414-1.414L10 10.828 5.757 6.586 4.343 8z"
                       />
                     </svg>
                   </span>
                 </button> --}}
                 {{-- <ul --}}
                   {{-- class="bg-white border rounded-sm transform scale-0 group-hover:scale-100 absolute 
                         transition duration-150 ease-in-out origin-top min-w-32"> --}}
                     {{-- <li class="px-2 py-2 hover:bg-gray-200">
                         <a href="{{route('users.index')}}">Users</a>
                     </li> --}}
                     {{-- <li class="px-3 py-2 hover:bg-gray-200">
                         <a href="{{route('roles.index')}}">Roles</a>
                     </li>
                     <li class="px-3 py-2 hover:bg-gray-200">
                         <a href="{{route('permission.index')}}">Permission</a>
                     </li> --}}
                   
                 {{-- </ul> --}}
               {{-- </div>
               
               <style>
                 /* since nested groupes are not supported we have to use 
                    regular css for the nested dropdowns 
                 */
                 li>ul                 { transform: translatex(100%) scale(0) }
                 li:hover>ul           { transform: translatex(101%) scale(1) }
                 li > button svg       { transform: rotate(-90deg) }
                 li:hover > button svg { transform: rotate(-270deg) }
               
                 .group:hover .group-hover\:scale-100 { transform: scale(1) }
                 .group:hover .group-hover\:-rotate-180 { transform: rotate(180deg) }
                 .scale-0 { transform: scale(0) }
                 .min-w-32 { min-width: 8rem }
               </style>
         </div> --}}

        <div class="flex flex-no-wrap items-center bg-white">
             <a class="flex py-4 px-3 hover:bg-gray-300" 
                 href="{{ route('logout') }}"
                 title="Выход"
                 onclick="event.preventDefault();
                 document.getElementById('logout-form').submit();">
                 <i class="fas fa-power-off"></i>
             </a>

             <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                 @csrf
             </form>
        </div>
     </div>
     
 </div>            
@endif