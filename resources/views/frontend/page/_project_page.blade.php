@extends('frontend.layouts.main', [
    'title' => $project->name.' | Личный проект',
    'description' => $project->description,
    'keywords' => 'проект, веб портфолио, Святослав Торн, выполненный заказ',
])

@section('content')
    <style>
        .gradient{
            background: rgb(131,131,131);
            background: linear-gradient(90deg, rgba(131,131,131,1) 0%, rgba(209,209,209,0.7010154403558299) 36%, rgba(142,142,142,1) 100%);
        }
        .gra-soc {
            background: linear-gradient(90deg, #d53369 0%, #daae51 100%);
        }
    </style>
@if (\Auth::check())
    @if (\Auth::user()->roles('admin'))
    <div class="absolute  text-gray-700 z-20" style="top: 5rem; left: 10.5rem" >
        <div class="flex flex-row justify-center">
            <a href="{{ route('projectes.edit', $project->id) }}" class="flex items-center bg-white py-2 px-8 hover:bg-gray-300">
                Редактировать
            </a>   
        </div>
    </div>

    @endif
@else
    <div class="absolute  text-white z-20" style="top: 5.5rem; left: 8.5rem" >
        <div class="flex flex-row justify-center">
            <a href="{{ route('projects') }}" class="flex items-center bg-transparent py-2 px-3">
                <i class="mr-2  fas fa-arrow-left"></i> <span class="">К списку</span>
            </a>   
        </div>
    </div>
@endif

    <span id="navFix" class="hidden"></span>

    <section class="gradient pt-12 md:pt-24 mt-2">

        <div class="container mx-auto flex items-start flex-wrap pt-4 pb-12">

            <div class="w-full order-1 md:w-4/6">
                {{-- Title project --}}
                <div class="w-full  px-3 md:px-6 py-4 flex flex-col">
                    <div class="bg-white p-3 md:p-4 text-black">
                        <h2 class="text-2xl">{{$project->name}}</h2>
                        <p class="text-bold">{{\Str::of($project->tech)->replace(' ', ', ')}}</p>
                    </div>
                </div>
                {{-- /Title project --}}

                <div class="w-full px-3 md:px-6 flex flex-col mb-4">
                    <div class="w-full bg-white text-black p-6">
                        {!!$project->description!!}
                    </div>
                </div>
                
                <div class="w-full px-3 md:px-6 flex flex-col mb-4">
                    <div class="w-full bg-white text-black">
                        <img class="w-full h-auto" src="/{!!$project->main_img!!}" alt="">
                    </div>
                </div>

                <div class="w-full px-3 md:px-6 flex flex-col">
                    <div class="w-full bg-white text-black p-6">
                        {!!$project->content!!}
                    </div>
                </div>

                {{-- Section project --}}
                {{-- <div class="w-full  px-3 md:px-6  flex flex-col">
                    <div class="bg-gray-800 px-3 md:px-4 py-4 text-white uppercase">
                        <p class="text-bold">Frontend Screenshots</p>
                    </div>
                </div> --}}
                {{-- /Section project --}}
                {{-- @for ($i = 0; $i < 6; $i++)
                    <div class="w-full  px-3 md:px-6 py-4  flex flex-col">
                        <a href="{{route('project.page', 1)}}" class="bg-white  hover:grow hover:shadow-lg">
                            <img class="w-full h-auto" src="https://images.unsplash.com/photo-1555982105-d25af4182e4e?ixlib=rb-1.2.1&amp;ixid=eyJhcHBfaWQiOjEyMDd9&amp;auto=format&amp;fit=crop&amp;w=400&amp;h=200&amp;q=80">
                        </a>
                    </div>
                    @if ($i == 2)
                    <div class="w-full  px-3 md:px-6  flex flex-col">
                        <div class="bg-gray-800 px-3 md:px-4 py-4 text-white uppercase">
                            <p class="text-bold">backend Screenshots</p>
                        </div>
                    </div>
                    @endif
                @endfor --}}
            </div>
            <div class="w-full h-full order-last md:w-2/6 sticky" style="top: 5rem">
                <div class="w-full  px-3 md:px-6 py-4  flex flex-col">
                    <div class="card bg-white shadow-lg p-6 text-black">
                        <img class="w-16 h-16 md:w-24 md:h-24 rounded-full mx-auto" src="https://sun9-38.userapi.com/c855016/v855016322/1b62d2/W4O_L3JRaHw.jpg?ava=1" alt="">
                        <div class="text-center mt-2">
                            <h2 class="text-2xl">Святослав Торн</h2>
                            <div class="opacity-75">Веб-разработчик</div>
                        </div>
                        <div class="text-center  mt-3 text-xl">
                            <a class="inline-block " href="https://t.me/svjatoslavtorn">
                                <i class="fab fa-telegram-plane bg-indigo-600 p-3 rounded text-white"></i>
                            </a>
                            {{-- <a class="inline-block " href="https://t.me/svjatoslavtorn">
                                <i class="fab fa-viber bg-indigo-600 p-3 rounded text-white"></i>
                            </a>
                            <a class="inline-block " href="https://t.me/svjatoslavtorn">
                                <i class="fab fa-whatsapp bg-indigo-600 p-3 rounded text-white"></i>
                            </a> --}}
                            <div class="inline-block" title="sefkiss.torn@yandex.ru">
                                <i class="far fa-envelope bg-indigo-600 p-3 rounded text-white"></i>
                            </div>
                        </div>
                        <div class="text-center mt-3">
                            <div class=" opacity-50">sefkiss.torn@yandex.ru</div>
                        </div>
                    </div>
                </div>
            </div>
            
            

        </div>

    </section>


@endsection