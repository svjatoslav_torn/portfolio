@extends('frontend.layouts.main', [
    'title' => $note->title.(\Str::length($note->title) < 15 ? ' | Заметки веб-разработчика' : ''),
    'description' => $note->seo_desc,
    'keywords' => $note->seo_key,
])

@section('content')
    <style>
        .gradient{
            background: rgb(131,131,131);
            background: linear-gradient(90deg, rgba(131,131,131,1) 0%, rgba(209,209,209,0.7010154403558299) 36%, rgba(142,142,142,1) 100%);
        }
    </style>
@if (\Auth::check())
    @if (\Auth::user()->roles('admin'))
    <div class="absolute  text-gray-700 z-20" style="top: 5rem; left: 10.5rem" >
    <div class="flex flex-row justify-center">
        <a href="{{ route('notez.edit', $note->id) }}" class="flex items-center bg-white py-2 px-8 hover:bg-gray-300">
            Редактировать
        </a>   
    </div>
    </div>
        
    @endif    

@else
<div class="hidden md:block absolute  text-white z-20" style="top: 5.5rem; left: 8.5rem" >
    <div class="flex flex-row justify-center">
        <a href="{{ route('notes') }}" class="flex items-center bg-transparent py-2 px-1">
            <i class="mr-2  fas fa-arrow-left"></i> <span class="">К списку</span>
        </a>   
    </div>
    </div>
@endif

    <span id="navFix" class="hidden"></span>

    <section class="gradient pt-12 md:pt-24">

        <div class="container mx-auto flex items-start flex-wrap pt-0 md:pt-4 pb-12 mt-0 md:mt-3">

            <div class="w-full order-1 lg:w-4/6">
                {{-- <div class="w-full  px-3 md:px-6 py-4  flex flex-col">
                    
                </div> --}}
                <div class="w-full flex flex-col items-center md:px-3">

                    <article class="w-full flex flex-col shadow-lg my-4 text-gray-700">
                        <!-- Article Image -->
                        @if($note->img_src)
                            <div class="w-full h-40 relative bg-no-repeat bg-cover" style="background-image: url('/{{$note->img_src}}')">
                                <div class="absolute bottom-0 right-0 py-1 px-2 flex items-center text-white text-xl">
                                    <i class="far fa-eye mr-2"></i>{{$note->view_counter}}
                                </div>
                            </div>
                        @endif
                        <div class="bg-white flex flex-col justify-start p-4 md:p-8 relative">
                            @if($note->img_src === null)
                                <div class="absolute top-0 right-0 py-2 px-3 flex items-center text-gray-500 text-lg md:text-xl">
                                    <i class="far fa-eye mr-2"></i>{{$note->view_counter}}
                                </div>
                            @endif
                            <a href="#" class="text-gray-500 text-sm uppercase pb-1">{{$note->main_tag}}</a>
                            <h1 class="text-xl sm:text-2xl md:text-3xl font-bold hover:text-gray-700 ">{{$note->title}}</h1>
                            <p href="#" class="text-sm pb-8 border-b border-gray-400">
                                @php
                                // var_dump(config('app.locale'));die;
                                // \Carbon\Carbon::setUTF8(true);
                                // \Carbon\Carbon::setLocale('ru_RU');
                                // setlocale(LC_TIME, 'ru_RU');
                                @endphp
                                
                                От <a href="#" class="font-semibold hover:text-gray-800">Святослав Торн</a>, публикация от {{\Carbon\Carbon::parse($note['created_at'])->formatLocalized('%e %B, %Y')}}
                            </p>
                            
                            <div class="py-6" id="article">
                                {!!$note['content']!!}
                            </div>
                        </div>
                    </article>
        
                    <div class="w-full flex shadow-lg text-gray-900 pt-6">
                        @foreach ($relatedNotes as $item)
                            <a href="#" class="w-1/2 flex bg-white p-6 items-center {{$loop->index == 0 ? 'border-r border-gray-300' : 'justify-end'}} hover:opacity-75">
                                <p class="text-lg text-blue-800 font-bold flex {{$loop->index == 1 ? 'order-last' : ''}}"><i class="fas {{$loop->index == 0 ? 'fa-chevron-left' : 'fa-chevron-right'}}"></i></p>
                                <p class="flex {{$loop->index == 0 ? 'ml-2 flex-1 justify-center' : 'flex-1 justify-center mr-2'}}">{{$item->title}}</p>
                            </a>                            
                        @endforeach
                    </div>
        
                    
        
                </div>


            </div>
            <div class="hidden lg:block w-full h-full order-last md:w-2/6 sticky" style="top: 5rem">
                <div class="w-full  px-3 md:px-6 py-4  flex flex-col">
                    <div class="card bg-white shadow-lg p-6 text-black">
                        <img class="w-16 h-16 md:w-24 md:h-24 rounded-full mx-auto" src="/img/profile-img.jpeg" alt="">
                        <div class="text-center mt-2">
                            <h2 class="text-2xl">Святослав Торн</h2>
                            <div class="text-gray-600">Веб-разработчик</div>
                            <div class="font-thin p-4 text-gray-500">
                                Привет, это мой мини блог о веб разработке. Возможно кому то будут полезны мои заметки.
                            </div>
                        </div>
                        <div class="text-center  mt-3 text-xl">
                            <a class="inline-block " href="https://t.me/svjatoslavtorn">
                                <i class="fab fa-telegram-plane bg-indigo-600 p-3 rounded text-white hover:opacity-75 transition ease-in-out duration-150"></i>
                            </a>
                            {{-- <a class="inline-block " href="https://t.me/svjatoslavtorn">
                                <i class="fab fa-viber bg-indigo-600 p-3 rounded text-white hover:opacity-75 transition ease-in-out duration-150"></i>
                            </a>
                            <a class="inline-block " href="https://t.me/svjatoslavtorn">
                                <i class="fab fa-whatsapp bg-indigo-600 p-3 rounded text-white hover:opacity-75 transition ease-in-out duration-150"></i>
                            </a> --}}
                            <div class="inline-block" title="sefkiss.torn@yandex.ru">
                                <i class="far fa-envelope bg-indigo-600 p-3 rounded text-white hover:opacity-75 transition ease-in-out duration-150"></i>
                            </div>
                        </div>
                        <div class="text-center mt-3">
                            <div class=" opacity-50">sefkiss.torn@yandex.ru</div>
                        </div>
                    </div>
                    {{-- <div class="card bg-white shadow-lg mt-3 p-6 text-black">
                        <div class="-m-2 flex flex-wrap">                            
                            <a href="#" class="hover:opacity-75 bg-indigo-600 text-white px-3 py-2 mt-1 mr-1 transition ease-in-out duration-150">PHP</a>
                            <a href="#" class="hover:opacity-75 bg-indigo-600 text-white px-3 py-2 mt-1 mr-1 transition ease-in-out duration-150">Yii2</a>
                            <a href="#" class="hover:opacity-75 bg-indigo-600 text-white px-3 py-2 mt-1 mr-1 transition ease-in-out duration-150">Laravel</a>
                            <a href="#" class="hover:opacity-75 bg-indigo-600 text-white px-3 py-2 mt-1 mr-1 transition ease-in-out duration-150">Css</a>
                            <a href="#" class="hover:opacity-75 bg-indigo-600 text-white px-3 py-2 mt-1 mr-1 transition ease-in-out duration-150">Disign</a>
                            <a href="#" class="hover:opacity-75 bg-indigo-600 text-white px-3 py-2 mt-1 mr-1 transition ease-in-out duration-150">HTML</a>
                            <a href="#" class="hover:opacity-75 bg-indigo-600 text-white px-3 py-2 mt-1 mr-1 transition ease-in-out duration-150">JS</a>
                            <a href="#" class="hover:opacity-75 bg-indigo-600 text-white px-3 py-2 mt-1 mr-1 transition ease-in-out duration-150">jQuery</a>
                            <a href="#" class="hover:opacity-75 bg-indigo-600 text-white px-3 py-2 mt-1 mr-1 transition ease-in-out duration-150">Vue.js</a>
                        </div>
                    </div> --}}
                </div>
            </div>
            
            

        </div>

    </section>


@endsection

@section('js')
    <script>
            $('#article').find('h1').addClass('text-extrabold text-xl sm:text-2xl md:text-3xl text-gray-700 py-2')
            $('#article').find('h2').addClass('text-bold text-xl md:text-2xl text-gray-600 py-2')
            $('#article').find('strong').addClass('bg-gray-300 px-1')
            $('#article').find('em').addClass('text-gray-600 p-2')
            $('#article').find('p').addClass('text-lg md:text-xl font-light text-gray-800 py-1 leading-normal md:leading-relaxed')
            $('#article').find('blockquote p').removeClass('text-lg md:text-xl text-gray-800').addClass('text-sm md:text-base italic font-light text-gray-600')
            $('#article').find('img').parent().addClass('-mx-4 md:-mx-8')
            $('#article').find('pre').addClass('-mx-4 md:-mx-8 my-2')
            $('#article').find('code').addClass('px-4 md:px-8 py-4')
            $('#article').find('img').addClass('w-full h-auto my-3')
            $('#article').find('a').parent().removeClass('text-gray-800')
            $('#article').find('a').addClass('text-gray-600 underline hover:opacity-75')
            $('#article').find('blockquote').prepend('<i class="mr-3 text-xl md:text-3xl fas fa-quote-right"></i>').addClass('flex items-center border-l-4 border-gray-400 p-3 text-gray-600')
    </script>
@endsection
