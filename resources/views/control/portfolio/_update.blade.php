@extends('frontend.layouts.main')

@section('content')
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/cropperjs/1.5.6/cropper.min.css">
<style>
    .gradient{
        background: rgb(131,131,131);
        background: linear-gradient(90deg, rgba(131,131,131,1) 0%, rgba(209,209,209,0.7010154403558299) 36%, rgba(142,142,142,1) 100%);
    }
</style>
<span id="navFix" class="hidden"></span>
<!--Hero-->
<div class="pt-6 md:pt-32">
    <div class="container px-3 mx-auto flex flex-wrap flex-col md:flex-row items-center">
      
    
        <div class="w-full leading-loose mb-3 flex justify-center">
            <form id="createPortForm" class="w-3/4 m-4 "  action="{{ route('portfolioz.update', $item->id) }}" method="POST" enctype="multipart/form-data">
              {!! csrf_field() !!}
              @method('PUT')
              <input type="hidden" name="refLink" value="{{$ref}}">
              <div class="w-full p-10 bg-white">
                <p class="text-gray-800 font-medium text-xl">Редактирование работы:</p>
                <div class="flex mt-4 mb-3">
                  <button class="px-4 w-full hover:bg-blue-600 py-1 text-white font-light tracking-wider bg-blue-500 rounded" 
                          type="submit">Обновить</button>
                </div>               
                <div class="flex flex-no-wrap">
                    <input class="w-full px-5 py-1 text-gray-700 bg-gray-200 mr-2" value="{{$item->name}}" title="Название" name="name" type="text" required="" placeholder="Название">
                </div>
                <div class="mt-2 flex flex-no-wrap items-center">
                  <input class="w-full px-5 py-1 text-gray-700 bg-gray-200 rounded mr-2" value="{{$item->seo_desc}}" title="SEO Description" name="seo_desc" type="text" required="" placeholder="SEO Description">
                  <input class="w-full px-5 py-1 text-gray-700 bg-gray-200 rounded" value="{{$item->seo_key}}" title="SEO Keywords" name="seo_key" type="text" required="" placeholder="SEO Keywords">
                </div>
                <div class="flex flex-no-wrap mt-3">
                    <input class="w-full px-5 py-1 text-gray-700 bg-gray-200 mr-2" value="{{$item->tech}}" title="Технологии" name="tech" type="text" required="" placeholder="Технологии">
                </div>

              </div>

              <div class="w-full p-10 bg-white mt-4">
                <div class="mt-2 flex flex-wrap items-center">
                  <div class="w-full">
                    <input class="w-full px-5 py-1 text-gray-700 bg-gray-200 my-2" value="{{$item->text}}" type="text" name="text" placeholder="Описание главного изображения">
                  </div>
                  <input type="file" class="bg-white text-black p-3 w-1/2" name="main_img" id="imgSrcUpload">
                  <input type="hidden" name="currentSrc" value="{{$item->main_img}}">   
                  <div class="w-full">
                    <img src="/{{$item->main_img}}" alt="">  
                  </div>   
                </div>
              </div>
              
              {{-- Only VIEW --}}
              @foreach (json_decode($item->images, true) as $it)
                <div class="option-image-{{$loop->index}} w-full px-10 py-4 bg-white mt-4">
                  <div class="mt-2 w-full flex flex-wrap items-center text-black justify-center">
                    <input type="hidden" name="{{$loop->index}}-src"> 
                    <div class="block" id="{{$loop->index}}-full">
                      <div class="w-full">
                        <input readonly class="w-full px-5 py-1 text-gray-700 bg-gray-200 my-2" value="{{$it['capture']}}" type="text" name="{{$loop->index}}-caption" placeholder="Описание изображения">
                      </div>
                      <div class="w-full 1-prev">
                        <img src="/{{$it['img_path']}}" class="w-full h-auto" alt="">
                      </div>
                    </div>
                  </div>
                </div>                  
              @endforeach

              

              {{-- <div id="editor"></div> --}}
              
              
            </form>
          </div>

          


    </div>
</div>
@endsection
@section('js')
    
{{-- <script src="https://cdn.ckeditor.com/4.14.0/standard/ckeditor.js"></script> --}}
{{-- <script src="{{ asset('ckeditor/ckeditor.js') }}"></script> --}}
{{-- <script src="{{ asset('js/jq_tailwind_checkbox.js') }}"></script> --}}
{{-- <script src="https://cdnjs.cloudflare.com/ajax/libs/cropperjs/1.5.6/cropper.min.js"></script> --}}


<script>

  window.onload = function () {
    // First event
    $('.option-image-1 input[name="1-image"]').on('change', function () {
      ajaxFunc(1);
      $('#1-full').removeClass('hidden'); //Show full info - image and input capture
      $('.option-image-2').removeClass('hidden'); //Show 2
      // Two event
      $('.option-image-2 input[name="2-image"]').on('change', function () {
        ajaxFunc(2);
        $('#2-full').removeClass('hidden'); //Show full info - image and input capture
        $('.option-image-3').removeClass('hidden'); //Show 2
        // Three event
        $('.option-image-3 input[name="3-image"]').on('change', function () {
          ajaxFunc(3);
          $('#3-full').removeClass('hidden'); //Show full info - image and input capture
        });
      });

    });

  }

  function ajaxFunc(number) {
    var file_data = $('input[name="'+number+'-image"]').prop('files')[0];
    var form_data = new FormData();
    form_data.append('file', file_data);
    $.ajax({
      type: "POST",
      url: "{{ route('notez.imageTempStore') }}",
      processData: false,
      cache: false,
      contentType: false,
      data: form_data,
      headers: {
          'X-CSRF-TOKEN': "{{ csrf_token() }}"
      },
      success: function (data) {
          // console.log('Ok');
          // console.log(data) 
          $('.'+number+'-prev').html(data['crop_block']);
          $('input[name="'+number+'-src"]').val(data['crop_base64']);         
      }
  });
  }  
</script>

@endsection
