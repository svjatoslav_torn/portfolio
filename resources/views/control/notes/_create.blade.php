@extends('frontend.layouts.main')

@section('content')
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/cropperjs/1.5.6/cropper.min.css">
<style>
    .gradient{
        background: rgb(131,131,131);
        background: linear-gradient(90deg, rgba(131,131,131,1) 0%, rgba(209,209,209,0.7010154403558299) 36%, rgba(142,142,142,1) 100%);
    }
</style>
<span id="navFix" class="hidden"></span>
<!--Hero-->
<div class="pt-6 md:pt-32">
    <div class="container px-3 mx-auto flex flex-wrap flex-col md:flex-row items-center">
      
    
        <div class="w-full leading-loose mb-3">
            <form id="createNoteForm" class="w-full m-4 "  action="{{ route('notez.store') }}" method="POST" enctype="multipart/form-data">
              {!! csrf_field() !!}
              <div class="w-full p-10 bg-white">
                <p class="text-gray-800 font-medium text-xl">Создание новой заметки:</p>
                <div class="mt-4">
                  <button class="px-4 w-full hover:bg-blue-600 py-1 text-white font-light tracking-wider bg-blue-500 rounded" type="submit">Добавить</button>
                </div>
                <div class="flex flex-no-wrap py-2 text-black">
                  <label for="statusToggle" class="mr-2">Опубликовать: </label><input class="px-3 border-double border-2 border-blue-600 text-white whitespace-no-wrap overflow-hidden w-8 h-8" type="checkbox" id="statusToggle" name="status" />
                  
                </div>
                <div class="flex flex-no-wrap">
                    <input class="w-full px-5 py-1 text-gray-700 bg-gray-200 mr-2" title="Заголовок" name="title" type="text" required="" placeholder="Заголовок">
                </div>
                <div class="flex flex-no-wrap mt-2">
                    <input class="w-full px-5 py-1 text-gray-700 bg-gray-200 mr-2" title="Текст для карточки" name="preview" type="text" required="" placeholder="Текст для карточки">
                </div>
                <div class="mt-2 flex flex-no-wrap items-center">
                  <input class="w-full px-5 py-1 text-gray-700 bg-gray-200 rounded mr-2" title="Основной тег заметки" name="main_tag" type="text" required="" placeholder="Основной тег заметки">
                  <input class="w-full px-5 py-1 text-gray-700 bg-gray-200 rounded" title="Время чтения заметки" name="read_time" type="number" required="" placeholder="Время чтения">
                </div>
                <div class="mt-2 flex flex-no-wrap items-center">
                  <input class="w-full px-5 py-1 text-gray-700 bg-gray-200 rounded mr-2" title="SEO Description" name="seo_desc" type="text" required="" placeholder="SEO Description">
                  <input class="w-full px-5 py-1 text-gray-700 bg-gray-200 rounded" title="SEO Keywords" name="seo_key" type="text" required="" placeholder="SEO Keywords">
                </div>

              </div>
              <div class="w-full p-10 bg-white mt-4">
                <div class="mt-2 flex flex-wrap items-center">
                  <span id="cutImage" class="text-center bg-white text-white bg-green-500 hover:bg-green-600 cursor-pointer p-2 border w-1/2">Обрезать изображение</span>
                  <input type="hidden" name="img_src" value="" id="src">
                  <input type="file" class="bg-white text-black p-3 w-1/2" name="upload" id="imgSrcUpload">
                  
                  <div class="w-1/2 " id="originalImage">
                  </div>
                  <div class="w-1/2 pl-2" id="cropperImage">
  
                  </div>
                </div>
              </div>
              
              <div class="w-full p-10 bg-white mt-4">
                <div class="mt-2 w-full flex flex-no-wrap items-center text-black px-16">
                  <textarea name="content" id="editor"  width="100%" class="w-full px-5 py-1 text-gray-700 bg-gray-200 rounded mr-2"></textarea>
                </div>

              </div>

              {{-- <div id="editor"></div> --}}
              
              
            </form>
          </div>

          


    </div>
</div>
@endsection
@section('js')
    
{{-- <script src="https://cdn.ckeditor.com/4.14.0/standard/ckeditor.js"></script> --}}
<script src="{{ asset('ckeditor/ckeditor.js') }}"></script>
{{-- <script src="{{ asset('js/jq_tailwind_checkbox.js') }}"></script> --}}
<script src="https://cdnjs.cloudflare.com/ajax/libs/cropperjs/1.5.6/cropper.min.js"></script>
<script>
  CKEDITOR.replace( 'editor', {
        filebrowserUploadUrl: "{{route('notez.imageSave', ['_token' => csrf_token() ])}}",
        filebrowserUploadMethod: 'form'
    });
   CKEDITOR.config.width = '100%';
   CKEDITOR.config.height = '100vh';
  //  CKEDITOR.replace('editor', {
  //       filebrowserUploadUrl: "",
  //       // filebrowserUploadMethod: 'form'
  //   });
</script>

<script>
  $('#imgSrcUpload').on('change', function () {
    var file_data = $('#imgSrcUpload').prop('files')[0];
    var form_data = new FormData();
    form_data.append('file', file_data);
          $.ajax({
            type: "POST",
            url: "{{ route('notez.imageTempStore') }}",
            processData: false,
            cache: false,
            contentType: false,
            data: form_data,
            headers: {
                'X-CSRF-TOKEN': "{{ csrf_token() }}"
            },
            success: function (data) {
                console.log(data);
                $('#originalImage').html(data['crop_block']);
                cropperInit();
            }
        });
  });

  function cropperInit(){
    const image = document.getElementById('image');
    const cropper = new Cropper(image, {
      aspectRatio: 16 / 9,
      scalable: false,
      zoomable: false,
    });

    $('#cutImage').on('click', function(){
      cropper.getCroppedCanvas({
        width: 1920,
        height: 1080,
        fillColor: '#fff',
        imageSmoothingEnabled: false,
        imageSmoothingQuality: 'medium',
      });
      cropper.getCroppedCanvas().toBlob((blob) => {
        const formData = new FormData();
      formData.append('file', blob);
      $.ajax({
            type: "POST",
            url: "{{ route('notez.imageTempStore') }}",
            processData: false,
            cache: false,
            contentType: false,
            data: formData,
            headers: {
                'X-CSRF-TOKEN': "{{ csrf_token() }}"
            },
            success: function (data) {
                console.log(data);
                $('#cropperImage').html(data['crop_block']);
                $('#src').val(data['crop_base64']);
            }
        });
      });
    });
  }
</script>

<script>
  // import Cropper from 'cropperjs';

// const image = document.getElementById('image');
// const cropper = new Cropper(image, {
//   aspectRatio: 16 / 9,
//   crop(event) {
//     console.log(event.detail.x);
//     console.log(event.detail.y);
//     console.log(event.detail.width);
//     console.log(event.detail.height);
//     console.log(event.detail.rotate);
//     console.log(event.detail.scaleX);
//     console.log(event.detail.scaleY);
//   },
// });
</script>
@endsection
