@extends('frontend.layouts.main')

@section('content')
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/cropperjs/1.5.6/cropper.min.css">
<style>
    .gradient{
        background: rgb(131,131,131);
        background: linear-gradient(90deg, rgba(131,131,131,1) 0%, rgba(209,209,209,0.7010154403558299) 36%, rgba(142,142,142,1) 100%);
    }
</style>
<span id="navFix" class="hidden"></span>



<div class="pt-6 md:pt-32">
    <div class="container px-3 mx-auto flex flex-wrap flex-col md:flex-row items-center">
      
    
      <div class="w-full leading-loose mb-3 flex justify-center">
        <form id="createPortForm" class="w-3/4 m-4 "  action="{{ route('projectes.update', $project->id) }}" method="POST" enctype="multipart/form-data">
          {!! csrf_field() !!}
          @method('PUT')
          <input type="hidden" name="refLink" value="{{$ref}}">
          <div class="w-full p-10 bg-white">
            <p class="text-gray-800 font-medium text-xl">Обновление проекта:</p>
            <div class="flex mt-4 mb-3">
              <button class="px-4 w-full hover:bg-blue-600 py-1 text-white font-light tracking-wider bg-blue-500 rounded" 
                      type="submit">Обновить</button>
            </div>               
            <div class="flex flex-no-wrap">
                <input class="w-full px-5 py-1 text-gray-700 bg-gray-200 mr-2" value="{{$project->name}}" title="Название проекта" name="name" type="text" required="" placeholder="Название проекта">
            </div>
            
            <div class="flex flex-no-wrap mt-3">
                <input class="w-full px-5 py-1 text-gray-700 bg-gray-200 mr-2" value="{{$project->description}}" title="Описание проекта" name="description" type="text" required="" placeholder="Краткое описание проекта">
            </div>
            
            <div class="flex flex-no-wrap mt-3">
                <input class="w-full px-5 py-1 text-gray-700 bg-gray-200 mr-2" value="{{$project->tech}}" title="Технологии" name="tech" type="text" required="" placeholder="Технологии">
            </div>

          </div>

          <div class="w-full p-10 bg-white mt-4">
            <div class="mt-2 flex flex-wrap items-center">
              <input type="file" class="bg-white text-black p-3 w-1/2" name="main_img" id="imgSrcUpload">  
              <input type="hidden" name="currentSrc" value="{{$project->main_img}}"> 
              <div class="w-full">
                <img src="/{{$project->main_img}}" alt="">
              </div>   
            </div>
          </div>
          
          <div class="w-full bg-white mt-4">
            <div class="mt-2 w-full flex flex-no-wrap items-center text-black ">
              <textarea name="content" id="editor"  width="100%" class="w-full px-5 py-1 text-gray-700 bg-gray-200 rounded mr-2">{{$project->content}}</textarea>
            </div>
          </div>
          
          
        </form>
      </div>


    </div>
</div>
@endsection
@section('js')

<script src="{{ asset('ckeditor/ckeditor.js') }}"></script>

<script>
  CKEDITOR.replace( 'editor', {
        filebrowserUploadUrl: "{{route('notez.imageSave', ['_token' => csrf_token() ])}}",
        filebrowserUploadMethod: 'form'
    });
   CKEDITOR.config.width = '100%';
   CKEDITOR.config.height = '100vh';
</script>
@endsection
