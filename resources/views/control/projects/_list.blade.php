@extends('frontend.layouts.main')

@section('content')
<style>
    .gradient{
        background: rgb(131,131,131);
        background: linear-gradient(90deg, rgba(131,131,131,1) 0%, rgba(209,209,209,0.7010154403558299) 36%, rgba(142,142,142,1) 100%);
    }
</style>


<span id="navFix" class="hidden"></span>
<!--Hero-->
<div class="pt-6 md:pt-32 pb-8 h-full">
    <div class="container px-3 mx-auto flex flex-wrap flex-col md:flex-row items-center">

        <table class="border-collapse w-full mt-4">
            <thead>
                <tr>
                    <th class="p-3 font-bold uppercase bg-gray-200 text-gray-600 border border-gray-300 hidden lg:table-cell">Дата</th>
                    <th class="p-3 font-bold uppercase bg-gray-200 text-gray-600 border border-gray-300 hidden lg:table-cell w-3/5">Название</th>
                    <th class="p-3 font-bold uppercase bg-gray-200 text-gray-600 border border-gray-300 hidden lg:table-cell"></th>
                </tr>
            </thead>
            <tbody>
                @foreach ($projects as $project)
                <tr class="bg-white lg:hover:bg-gray-100 flex lg:table-row flex-row lg:flex-row flex-wrap lg:flex-no-wrap mb-10 lg:mb-0">
                    <td class="w-full lg:w-auto p-3 text-gray-800 text-center border border-b block lg:table-cell relative lg:static">
                        {{\Carbon\Carbon::parse($project['created_at'])->format('d.m.Y')}}
                    </td>
                    <td class="w-full lg:w-auto p-3 text-gray-800 text-center border border-b block lg:table-cell relative lg:static">
                        {{$project->name}}
                    </td>
                    <td class="w-full lg:w-auto p-3 text-gray-800 text-center border border-b text-center block lg:table-cell relative lg:static">
                        <form action="{{ route('projectes.destroy', $project->id) }}" method="POST">
                            @csrf
                            @method('DELETE')
       
                            <a href="{{route('projectes.edit', $project->id)}}" class="text-green-400 hover:text-blue-600 underline" title="Редактировать проект"><i class="far fa-edit"></i></a>
                            <button type="submit" class="text-red-400 hover:text-blue-600 underline pl-6" title="Удалить проект"><i class="far fa-trash-alt"></i></button>
                        </form>
                    </td>
                </tr>
                    
                @endforeach
                
            </tbody>
        </table>

    </div>
</div>

@endsection