<?php

/** @var  \Illuminate\Database\Eloquent\Factory $factory */
$factory->define(Brackets\AdminAuth\Models\AdminUser::class, function (Faker\Generator $faker) {
    return [
        'first_name' => $faker->firstName,
        'last_name' => $faker->lastName,
        'email' => $faker->email,
        'password' => bcrypt($faker->password),
        'remember_token' => null,
        'activated' => true,
        'forbidden' => $faker->boolean(),
        'language' => 'en',
        'deleted_at' => null,
        'created_at' => $faker->dateTime,
        'updated_at' => $faker->dateTime,
        
    ];
});/** @var  \Illuminate\Database\Eloquent\Factory $factory */
$factory->define(App\Models\Note::class, static function (Faker\Generator $faker) {
    return [
        'user_id' => $faker->sentence,
        'content' => $faker->text(),
        'title' => $faker->sentence,
        'seo_desc' => $faker->sentence,
        'seo_key' => $faker->sentence,
        'img_src' => $faker->sentence,
        'view_counter' => $faker->randomNumber(5),
        'created_at' => $faker->dateTime,
        'updated_at' => $faker->dateTime,
        'slug' => $faker->unique()->slug,
        
        
    ];
});
