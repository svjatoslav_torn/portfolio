<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Notes extends Model{
    // 

    public $fillable = [
        'title', 'content', 'seo_desc', 'seo_key', 'img_src', 'slug', 'user_id', 'status', 'preview', 'read_time', 'main_tag'
    ];

    // public function registerMediaCollections()
    // {
    //     $this->addMediaCollection('img_src');
    // }

    // public function registerMediaConversions(Media $media = null)
    // {
    //     $this->addMediaConversion('detail_hd')
    //         ->width(1920)
    //         ->height(1080)
    //         ->performOnCollections('gallery');
    // }
}
