<?php

namespace App\Http\Controllers;

use App\Notes;
use Illuminate\Http\Request;
// use Illuminate\Support\Facades\Input;
// use Illuminate\Http\UploadedFile;

class NotesController extends Controller
{

    /**
     * Display a listing of the resource for frontend.
     *
     * @return \Illuminate\Http\Response
     */
    public function indexFront()
    {
//        var_dump(\Auth::user()->hasRole('admin'));die;

        return view('frontend.notes', [
            'notes' => Notes::
                orderBy('created_at', 'desc')
                ->where(['status' => true])
                ->get()
                ->take(7),
        ]);
    }

    public function showFront($slug)
    {
        // setlocale(LC_TIME, 'ru');
        // \Carbon\Carbon::setLocale('ru');
        // var_dump(Notes::inRandomOrder()->limit(2)->get());die;
        // $note = Notes::find($id);
        $note = Notes::where(['slug' => $slug])->first();
        // var_dump(strftime('%e %B, %Y', strtotime($note->created_at)));die;
        $note->increment('view_counter');
        return view('frontend.page._note_page', [
            'note' => $note,
            'relatedNotes' => Notes::inRandomOrder()->limit(2)->get(),
        ]);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $notes = Notes::orderBy('id', 'desc')->get();
        return view('control.notes._note_list', [
            'notes' => $notes,
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('control.notes._create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if($request->img_src){
            $data = explode(',', $request->img_src);
            $img = base64_decode($data[1]);
            $path = 'posts/'.\Str::random(30).'.jpeg';
            file_put_contents($path, $img);
        }else{
            $path = null;
        }

        Notes::create([
            'user_id' => \Auth::user()->id,
            'title' => $request->get('title'),
            'read_time' => $request->input('read_time'),
            'main_tag' => $request->input('main_tag'),
            'img_src' => $path,
            'slug' => \Str::slug($request->get('title')),
            'seo_desc' => $request->get('seo_desc'),
            'preview' => $request->input('preview'),
            'seo_key' => $request->get('seo_key'),
            'content' => $request->get('content'),
            'status' => $request->get('status') == null ? 0 : 1,            
        ]);

        return redirect()->route('notez.index');
    }

    public function imageTempStore(Request $request)
    {
        $base64 = 'data:image/'.$request->file('file')->extension().';base64,' . base64_encode(file_get_contents($request->file('file')->path()));
        return [
            'crop_block' => '<img class="w-full h-auto" id="image" src="'.$base64.'" alt="image">',
            'crop_base64' => $base64,
        ];
    }

    // Ajax method
    // request - image from CKEditor
    // return SRC for uploaded image
    public function imageStore(Request $request)
    {
        // var_dump($request->get('CKEditorFuncNum'));die;

        $CKEditor = $request->input('CKEditor');
        $funcNum  = $request->input('CKEditorFuncNum');
        $message  = $url = '';
        // var_dump($funcNum);die;
        if ($request->hasFile('upload')) {
            // var_dump('Yeas');die;
            $file = $request->file('upload');
            if ($file->isValid()) {
                // $filename =rand(1000,9999).$file->getClientOriginalName();
                // $file->move(public_path().'/wysiwyg/', $filename);
                // $url = url('wysiwyg/' . $filename);
                
                $filename = \Str::random(15).$file->getClientOriginalName();
                $url = url('wysiwyg/' . $filename);
                $imageIm = \Image::make($file);
                if($imageIm->width() > 1921){
                    $imageIm->resize(1920, null, function ($constraint) {
                        $constraint->aspectRatio();
                    });
                }
                $imageIm->save(\public_path().'/wysiwyg/'.$filename, 80, 'jpg');
//                ->insert('img/water.png', 'bottom-left', 25, 0)
                
            } else {
                $message = 'An error occurred while uploading the file.';
            }
        } else {
            // var_dump('No');die;
            $message = 'No file uploaded.';
        }
        return '<script>window.parent.CKEDITOR.tools.callFunction('.$funcNum.', "'.$url.'", "'.$message.'")</script>';
    }

 /**
     * Display the specified resource.
     *
     * @param  \App\Portfolio  $portfolio
     * @return \Illuminate\Http\Response
     */
    public function show(Notes $notes)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Notes  $notes
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        // \var_dump($id);die;
        $note = Notes::find($id);
        // \var_dump($note->toArray());die;
        return view('control.notes._update_note', [
            'note' => $note,
            'ref' => \Request::server('HTTP_REFERER'),
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Notes  $notes
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(Request $request, $id)
    {
        if (strlen($request->get('img_src')) > 100 ) {
            $data = explode(',', $request->img_src);
            $img = base64_decode($data[1]);
            $path = 'posts/'.\Str::random(30).'.jpeg';
            file_put_contents($path, $img);
        }else{
            $path = $request->get('img_src');
        }
// var_dump($id);die;
        Notes::find($id)->update([
            'title' => $request->get('title'),            
            'read_time' => $request->input('read_time'),
            'main_tag' => $request->input('main_tag'),
            'preview' => $request->input('preview'),
            'img_src' => $path,
            'slug' => \Str::slug($request->get('title')),
            'seo_desc' => $request->get('seo_desc'),
            'seo_key' => $request->get('seo_key'),
            'content' => $request->get('content'),
            'status' => $request->get('status') == null ? 0 : 1,
        ]);
        // Toastr::success('Сток успешно отредактирован', 'Йууухуууу');

        if(\Str::contains($request->input('refLink'), 'notes/')){
            return redirect()->route('note.page', \Str::afterLast($request->input('refLink'), '/'));
        }

        return redirect()->route('notez.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Notes  $notes
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $note = Notes::find($id);

        if(file_exists($note->img_src)) unlink($note->img_src);

        \preg_match_all('/wysiwyg.([a-z0-9_\.-]+)/', $note->content, $m, PREG_SET_ORDER, 0);
        foreach ($m as $value) {
            if(\file_exists($value[0])) \unlink($value[0]);
        }

        $note->delete();

        return redirect()->route('notez.index');

    }
}
