<?php

namespace App\Http\Controllers;

use App\Portfolio;
use Illuminate\Http\Request;

class PortfolioController extends Controller
{

    /**
     * Display a listing of the resource for frontend.
     *
     * @return \Illuminate\Http\Response
     */
    public function indexFront()
    {
        return view('frontend.portfolio', [
            'portfolio' => Portfolio::orderBy('created_at', 'desc')->get()->take(24),
        ]);
    }

    public function showFront($id)
    {
        return view('frontend.page._portfolio_page', [
            'work' => Portfolio::find($id),
        ]);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $items = Portfolio::orderBy('id', 'desc')->get();
        return view('control.portfolio._portfolio_list', [
            'items' => $items,
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('control.portfolio._create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $images_array = [];

        for ($i=1; $i < 4; $i++) { 
            if(strlen($request->input($i.'-src')) > 100){
                $images_array[] = [
                    'capture' => $request->input($i.'-capture'),
                    'img_path' => $this->getImagePath($request->input($i.'-src')),
                ];
                // var_dump($images_array);die;
            }
        }

        // var_dump(\json_encode($images_array));die;
        if($request->file('main_img')->isValid()){
            // $path = $request->file('main_img')->store('portfolio');
            $path = 'portfolio/'.\Str::random(20).'.jpg';
            $img = \Image::make($request->file('main_img'))->save($path, 80, 'jpg');
        }
      
        Portfolio::create([
            'name' => $request->input('name'),
            'tech' => $request->input('tech'),
            'seo_desc' => $request->input('seo_desc'),
            'seo_key' => $request->input('seo_key'),
            'text' => $request->input('text'),
            'slug' => \Str::slug($request->input('name')),
            'main_img' => $path,
            'images' => json_encode($images_array),
        ]);

        return redirect()->route('portfolioz.index');
    }

    public function getImagePath($base64)
    {
        $data = explode(',', $base64);
        $path = 'portfolio/'.\Str::random(20).'.jpg';
        $img = \Image::make($data[1])->save($path, 80, 'jpg');

        return $path;
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Portfolio  $portfolio
     * @return \Illuminate\Http\Response
     */
    public function show(Portfolio $portfolio)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Portfolio  $portfolio
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        // var_dump($portfolio->name);die;
        $item = Portfolio::find($id);
        return view('control.portfolio._update', [
            'item' => $item,
            'ref' => \Request::server('HTTP_REFERER'),
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Portfolio  $portfolio
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        // var_dump($request->hasFile('main_img'));die;
        if($request->hasFile('main_img')){
            if($request->file('main_img')->isValid()){
                // $path = $request->file('main_img')->store('portfolio');
                $path = 'portfolio/'.\Str::random(20).'.jpg';
                $img = \Image::make($request->file('main_img'))->save($path, 80, 'jpg');
                unlink($request->input('currentSrc'));
            }
        }else{
            $path = $request->input('currentSrc');
        }
      
        Portfolio::find($id)
            ->update([
                'name' => $request->input('name'),
                'tech' => $request->input('tech'),
                'seo_desc' => $request->input('seo_desc'),
                'seo_key' => $request->input('seo_key'),
                'text' => $request->input('text'),
                'slug' => \Str::slug($request->input('name')),
                'main_img' => $path,
                // 'images' => json_encode($images_array),
            ]);

        if(\Str::contains($request->input('refLink'), 'myportfolio/')){
            return redirect()->route('portfolio.page', \Str::afterLast($request->input('refLink'), '/'));
        }

        return redirect()->route('portfolioz.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Portfolio  $portfolio
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $portfolio = Portfolio::find($id);

        if(file_exists($portfolio->main_img)) unlink($portfolio->main_img);
        
        foreach (json_decode($portfolio->images, true) as $value) {
            if(file_exists($value['img_path'])) unlink($value['img_path']);
        }

        $portfolio->delete();

        return redirect()->route('portfolioz.index');
        // var_dump('Есть');die;
    }
}
