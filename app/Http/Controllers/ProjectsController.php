<?php

namespace App\Http\Controllers;

use App\Projects;
use Illuminate\Http\Request;

class ProjectsController extends Controller
{

    /**
     * Display a listing of the resource for frontend.
     *
     * @return \Illuminate\Http\Response
     */
    public function indexFront()
    {
        $projects = Projects::orderBy('created_at', 'desc')->get()->take(9);

        return view('frontend.projects', [
            'projects' => $projects,
        ]);
    }

    public function showFront($slug)
    {
        return view('frontend.page._project_page', [
            'project' => Projects::where(['slug' => $slug])->first(),
        ]);
    }



    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $projects = Projects::orderBy('created_at', 'desc')->get();

        return view('control.projects._list', [
            'projects' => $projects,
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('control.projects._create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if($request->file('main_img')->isValid()){
            // $path = $request->file('main_img')->store('portfolio');
            $path = 'portfolio/'.\Str::random(20).'.jpg';
            $img = \Image::make($request->file('main_img'))->save($path, 80, 'jpg');
        }

        Projects::create([
            'name' => $request->input('name'),
            'tech' => $request->input('tech'),
            'description' => $request->input('description'),
            'content' => $request->input('content'),
            'slug' => \Str::slug($request->input('name')),
            'main_img' => $path,           
        ]);

        return redirect()->route('projectes.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Projects  $projects
     * @return \Illuminate\Http\Response
     */
    public function show(Projects $projects)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Projects  $projects
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        // var_dump(\Str::afterLast(\Request::server('HTTP_REFERER'), '/'));die;
        return view('control.projects._update', [
            'project' => Projects::find($id),
            'ref' => \Request::server('HTTP_REFERER'),
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Projects  $projects
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        if($request->hasFile('main_img')){
            if($request->file('main_img')->isValid()){
                // $path = $request->file('main_img')->store('portfolio');
                $path = 'portfolio/'.\Str::random(20).'.jpg';
                $img = \Image::make($request->file('main_img'))->save($path, 80, 'jpg');
                unlink($request->input('currentSrc'));
            }
        }else{
            $path = $request->input('currentSrc');
        }
      
        Projects::find($id)
            ->update([
                'name' => $request->input('name'),
                'tech' => $request->input('tech'),
                'description' => $request->input('description'),
                'content' => $request->input('content'),
                'slug' => \Str::slug($request->input('name')),
                'main_img' => $path,
            ]);
       
        if(\Str::contains($request->input('refLink'), 'projects/')){
            return redirect()->route('project.page', \Str::afterLast($request->input('refLink'), '/'));
        }
        return redirect()->route('projectes.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Projects  $projects
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $project = Projects::find($id);
        if(unlink($project->main_img)){

            \preg_match_all('/wysiwyg.([a-z0-9_\.-]+)/', $project->content, $m, PREG_SET_ORDER, 0);
            foreach ($m as $value) {
                if(\file_exists($value[0])) \unlink($value[0]);
            }

            $project->delete();
        }
        return redirect()->route('projectes.index');
    }
}
