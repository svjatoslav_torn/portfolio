<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Projects extends Model
{
    public $fillable = [
        'name', 'slug', 'tech', 'description', 'main_img', 'content',
    ];
}
