<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Portfolio extends Model
{
    public $fillable = [
        'name', 'tech', 'main_img', 'images', 'seo_desc', 'seo_key', 'text', 'slug',
    ];
}
